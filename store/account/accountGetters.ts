import { AccountStateDto } from '~/dto/account.state.dto'

export default {
  account: (state: AccountStateDto) => state,
}

import state from './accountState'
import getters from './accountGetters'
import mutations from './accountMutations'
import actions from './accountActions'

export default {
  state,
  getters,
  mutations,
  actions,
}

import * as types from './accountTypes'
import { httpService } from '~/services'
import { API_LOGIN } from '~/constants'
import { AccountDto } from '~/dto/account.dto'

export default {
  [types.ACCOUNT_REQUEST]: async (
    { commit }: { commit: Function },
    params: {}
  ) => {
    commit(types.ACCOUNT_REQUEST)

    try {
      const { data } = await httpService.post(API_LOGIN, params)
      commit(types.ACCOUNT_SUCCESS, data.data)
    } catch (error) {
      commit(types.ACCOUNT_ERROR, error)
    }
  },

  [types.ACCOUNT_CLEAR]: (
    { commit }: { commit: Function },
    response: AccountDto
  ) => {
    commit(types.ACCOUNT_CLEAR, response)
  },
}

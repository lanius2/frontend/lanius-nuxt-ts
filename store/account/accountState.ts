import { AccountStateDto } from '~/dto/account.state.dto'

export default () => new AccountStateDto()

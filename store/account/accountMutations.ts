import * as types from './accountTypes'
import { AccountStateDto } from '~/dto/account.state.dto'
import { setTokenToLocal } from '~/middleware/authToken'
import { AccountDto } from '~/dto/account.dto'

export default {
  [types.ACCOUNT_REQUEST]: (state: AccountStateDto) => {
    if (process.browser) {
      setTokenToLocal('')
    }
    state.isFetching = true
    state.isError = false
  },

  [types.ACCOUNT_SUCCESS]: (state: AccountStateDto, response: AccountDto) => {
    state.data = response
    if (process.browser) {
      setTokenToLocal(response.token)
    }
    state.isFetching = false
  },

  [types.ACCOUNT_ERROR]: (state: AccountStateDto, response: Error) => {
    state.isError = response
    state.isFetching = false
  },

  [types.ACCOUNT_CLEAR]: (state: AccountStateDto, response: AccountDto) => {
    state.data = response
    state.isFetching = false
  },
}

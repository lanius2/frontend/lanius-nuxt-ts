import { UsersStateDto } from '~/dto/users.state.dto'

export default {
  users: (state: UsersStateDto) => state,
}

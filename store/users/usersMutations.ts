import * as types from './usersTypes'
import { UserDto } from '~/dto/user.dto'
import { UsersStateDto } from '~/dto/users.state.dto'

export default {
  [types.USERS_REQUEST]: (state: UsersStateDto) => {
    state.isFetching = true
    state.isError = false
  },

  [types.USERS_SUCCESS]: (state: UsersStateDto, response: UserDto[]) => {
    state.data = response
    state.isFetching = false
  },

  [types.USERS_ERROR]: (state: UsersStateDto, response: Error) => {
    state.isError = response
    state.isFetching = false
  },
}

import * as types from './usersTypes'
import { httpService } from '~/services'
import { API_URL_PARAM, API_USER } from '~/constants'
import { UserDto } from '~/dto/user.dto'

export default {
  [types.USERS_REQUEST]: async (
    { commit }: { commit: Function },
    params: {}
  ) => {
    commit(types.USERS_REQUEST)

    try {
      const { data } = await httpService.get(API_USER, params)

      commit(types.USERS_SUCCESS, data.data)
    } catch (error) {
      commit(types.USERS_ERROR, error)
    }
  },

  [types.ADD_USER_REQUEST]: async (
    { commit }: { commit: Function },
    params: UserDto
  ) => {
    commit(types.ADD_USER_REQUEST)

    try {
      const inputData = {
        email: params.email,
        fullName: params.fullName,
        password: params.password,
        phone: params.phone,
        address: params.address,
      }
      const { data } = await httpService.post(API_USER, inputData)

      commit(types.USERS_SUCCESS, data.data)
    } catch (error) {
      commit(types.USERS_ERROR, error)
    }
  },

  [types.DELETE_USER_REQUEST]: async (
    { commit }: { commit: Function },
    userId: number
  ) => {
    commit(types.DELETE_USER_REQUEST)

    try {
      const url = API_USER + API_URL_PARAM(userId)
      const { data } = await httpService.delete(url)

      commit(types.USERS_SUCCESS, data.data)
    } catch (error) {
      commit(types.USERS_ERROR, error)
    }
  },

  [types.UPDATE_USER_REQUEST]: async (
    { commit }: { commit: Function },
    params: UserDto
  ) => {
    commit(types.UPDATE_USER_REQUEST)

    try {
      let inputData = {}
      if (params.password) {
        inputData = {
          email: params.email,
          fullName: params.fullName,
          password: params.password,
          phone: params.phone,
          address: params.address,
        }
      } else {
        inputData = {
          email: params.email,
          fullName: params.fullName,
          phone: params.phone,
          address: params.address,
        }
      }
      const url = API_USER + API_URL_PARAM(params.id)
      const { data } = await httpService.put(url, inputData)

      commit(types.USERS_SUCCESS, data.data)
    } catch (error) {
      commit(types.USERS_ERROR, error)
    }
  },
}

import { Store } from 'vuex'
import Account from './account'
import Users from './users'
import UserDetail from './userDetail'

const createStore = () => {
  return new Store({
    modules: {
      Account,
      Users,
      UserDetail,
    },
  })
}

export default createStore

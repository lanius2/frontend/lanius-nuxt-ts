import * as types from './userDetailTypes'
import { httpService } from '~/services'
import { API_URL_PARAM, API_USER } from '~/constants'
import { UserDto } from '~/dto/user.dto'

export default {
  [types.USER_DETAIL_REQUEST]: async (
    { commit }: { commit: Function },
    userId: number
  ) => {
    commit(types.USER_DETAIL_REQUEST)

    try {
      const url = API_USER + API_URL_PARAM(userId)
      const { data } = await httpService.get(url)

      commit(types.USER_DETAIL_SUCCESS, data.data)
    } catch (error) {
      commit(types.USER_DETAIL_ERROR, error)
    }
  },

  [types.USER_DETAIL_CLEAR]: (
    { commit }: { commit: Function },
    response: UserDto
  ) => {
    commit(types.USER_DETAIL_CLEAR, response)
  },
}

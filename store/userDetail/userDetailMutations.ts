import * as types from './userDetailTypes'
import { UserDto } from '~/dto/user.dto'
import { UserDetailStateDto } from '~/dto/userDetail.state.dto'

export default {
  [types.USER_DETAIL_REQUEST]: (state: UserDetailStateDto) => {
    state.isFetching = true
    state.isError = false
  },

  [types.USER_DETAIL_SUCCESS]: (
    state: UserDetailStateDto,
    response: UserDto
  ) => {
    state.data = response
    state.isFetching = false
  },

  [types.USER_DETAIL_ERROR]: (state: UserDetailStateDto, response: Error) => {
    state.isError = response
    state.isFetching = false
  },

  [types.USER_DETAIL_CLEAR]: (state: UserDetailStateDto, response: UserDto) => {
    console.log(response)
    state.data = response
    state.isFetching = false
  },
}

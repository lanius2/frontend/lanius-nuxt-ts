export const USER_DETAIL_REQUEST = 'USER_DETAIL_REQUEST'
export const USER_DETAIL_ERROR = 'USER_DETAIL_ERROR'
export const USER_DETAIL_SUCCESS = 'USER_DETAIL_SUCCESS'
export const USER_DETAIL_CLEAR = 'USER_DETAIL_CLEAR'

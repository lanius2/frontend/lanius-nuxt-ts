import { UsersStateDto } from '~/dto/users.state.dto'

export default () => new UsersStateDto()

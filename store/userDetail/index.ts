import state from './userDetailState'
import getters from './userDetailGetters'
import mutations from './userDetailMutations'
import actions from './userDetailActions'

export default {
  state,
  getters,
  mutations,
  actions,
}

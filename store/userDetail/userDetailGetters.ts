import { UserDetailStateDto } from '~/dto/userDetail.state.dto'

export default {
  userDetail: (state: UserDetailStateDto) => state,
}

export default function ({ $axios, store, redirect }) {
  console.log('AXIOS PLUGIN LOADED', $axios)

  $axios.onRequest((request) => {
    console.log('[ REQUEST ]' + request.url)
    if (store.state.account.data.token) {
      // eslint-disable-next-line dot-notation
      request.headers.common['Auth'] = 'JANCOK'
    }

    return request
  })

  $axios.onResponse((response) => {
    console.log('[ RESPONSE ]' + response.request.responseURL, response)
    // TODO: If token expires, perform a silent refresh

    return response
  })

  $axios.onError((error) => {
    console.error('[ ERROR ]', error)
    const code = parseInt(error.response && error.response.status)
    if (code === 401) {
      store.state.sessionStorage.authUser = null
      return redirect('/')
    }

    return error
  })
}

export const setTokenToLocal = (token: string) => {
  localStorage.setItem('token', token)
}

export const getTokenFromLocal = () => {
  if (process.browser) {
    const token = localStorage.getItem('token') || ''
    return token
  }
}

export const API_LOGIN = '/login'
export const API_USER = '/users'
export const API_URL_PARAM = (slug: string | number) => `/${slug}`

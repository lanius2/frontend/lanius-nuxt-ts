import { UserDto } from './user.dto'

export class UsersStateDto {
  public data: UserDto[] = []

  public isError: any = undefined

  public isFetching: boolean = false
}

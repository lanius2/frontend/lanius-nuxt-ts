import { UserDto } from './user.dto'

export class UserDetailStateDto {
  public data: UserDto = new UserDto()

  public isError: any = undefined

  public isFetching: boolean = false
}

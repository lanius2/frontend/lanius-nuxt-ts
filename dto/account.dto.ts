import { UserDto } from './user.dto'

export class AccountDto extends UserDto {
  token: string = ''
}

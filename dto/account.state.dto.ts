import { AccountDto } from './account.dto'

export class AccountStateDto {
  public data: AccountDto = new AccountDto()

  public isError: any = undefined

  public isFetching: boolean = false
}

export class UserDto {
  public id: string = ''

  public email: string = ''

  public password: string = ''

  public fullName: string = ''

  public phone: string = ''

  public address: string = ''
}

import axios from 'axios'
import { getTokenFromLocal } from '~/middleware/authToken'

export default class Http {
  protected axios

  public constructor(axiosConfig: any) {
    this.axios = axios.create(axiosConfig)
    this.axios.interceptors.request.use((config) => {
      if (getTokenFromLocal()) {
        // eslint-disable-next-line dot-notation
        config.headers['auth'] = getTokenFromLocal()
      }
      return config
    })
  }

  public get(url: string, params?: {}, responseType?: string) {
    // eslint-disable-next-line no-useless-catch
    try {
      const config: any = {
        method: 'get',
        url,
        params,
        withCredentials: true,
      }

      if (responseType) {
        config.responseType = responseType
      }

      return this.axios.request(config)
    } catch (error) {
      throw error
    }
  }

  public post(url: string, data: {}) {
    // eslint-disable-next-line no-useless-catch
    try {
      return this.axios.request({
        method: 'post',
        url,
        data,
        withCredentials: true,
      })
    } catch (error) {
      throw error
    }
  }

  public put(url: string, data: {}) {
    // eslint-disable-next-line no-useless-catch
    try {
      return this.axios.request({
        method: 'put',
        url,
        data,
        withCredentials: true,
      })
    } catch (error) {
      throw error
    }
  }

  public patch(url: string, data: {}) {
    // eslint-disable-next-line no-useless-catch
    try {
      return this.axios.request({
        method: 'patch',
        url,
        data,
        withCredentials: true,
      })
    } catch (error) {
      throw error
    }
  }

  public delete(url: string) {
    // eslint-disable-next-line no-useless-catch
    try {
      return this.axios.request({
        method: 'delete',
        url,
        withCredentials: true,
      })
    } catch (error) {
      throw error
    }
  }
}

import Http from './http'

export const httpService = new Http({
  timeout: 20000,
  baseURL: process.env.API_URL,
})
